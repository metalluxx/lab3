#include "containslist.h"
#include <QtDebug>

ContainsList::ContainsList(QListWidget *wprt) : QListWidget (wprt)
{
    this->setObjectName(QStringLiteral("saveList"));
    this->setMinimumSize(QSize(350, 0)); // установка фиксированной ширины
    this->setMaximumSize(QSize(350, 16777215));
}

bool QListWidgetItem::operator<(const QListWidgetItem &right)const{

    int ct2 = right.data(CONTAINS_CATEGORY).toInt(); //преобразование QVariant
    int ct1 = this->data(CONTAINS_CATEGORY).toInt();
    int cp2 = right.data(CONTAINS_PERIOD).toInt();
    int cp1 = this->data(CONTAINS_PERIOD).toInt();
    int cs2 = right.data(CONTAINS_INDEX).toInt();
    int cs1 = this->data(CONTAINS_INDEX).toInt();


    if(ct1 == ct2) {
        if(cp1 == cp2){
            if(cs1 == cs2)
            {
                return false;
            }
            else return  cs1 < cs2;
        }
        else return cp1 < cp2;
    }
    else return  ct1 < ct2;



}
