#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "contains.h" //класс для хранения данных

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{   ui->setupUi(this);
    MainWindow::setWindowFlags(Qt::WindowMaximizeButtonHint);// скрыть кнопку развертывания окна
    uncheckAllPer(); // отчистка ui
    ui->dateEdit->setDate(QDate::currentDate());
    ui->dateEdit->setMaximumDate(QDate::currentDate());//изменение диапазона для виджета даты
    ui->dateEdit->setMinimumDate(QDate::currentDate().addYears(-200));
}


MainWindow::~MainWindow() // деструктор
{
    delete ui;
    delete listData; //удаление контейнера из кучи
}

Contains MainWindow::getContains(int search_id) // получение элемента по id
{
    Contains tmp;
    foreach(tmp, *listData){
        if(tmp.id == search_id) // поиск нужного элемента по id
            return tmp;
    }
    return tmp;
};

void MainWindow::on_itemDelete_triggered()  //удаление элемента из списка
{
    accept_read = false;
    if(ui->saveList->currentRow() != -1)
    {
        for(int i = 0; i < listData->size(); ++i){
            if(listData->at(i).id == ui->saveList->currentItem()->data(CONTAINS_ID)) // поиск нужного элемента по id
            {
                listData->removeAt(i); // и его удаление
                break;
            }
        }
        delete ui->saveList->currentItem();
    }

    if(ui->saveList->count()==0) // загрузка пустого итема при пустом списке
    {
        Contains tmp;
        readData(&tmp);
        enableui(false);
    }
}

void MainWindow::on_itemAdd_triggered() // добавление элемента в список
{
    accept_read = false;

    enableui(true);
    Contains *curItem = new Contains;
    listData->append(*curItem); // добавление новой записи в массив
    int wid = getCorrectRandomId();
    listData->back().id = wid; // и запись id итема
    delete  curItem;
    updateList(wid);

    accept_read = true;
}


void MainWindow::savechanges(){
    if((ui->saveList->count() != 0) and (accept_read)){
        Contains *tmp = new Contains();
        writeData(tmp); //запись в временную переменную данных интерфейса
        tmp->id = wid;
        for(int i = 0; i < listData->size(); ++i) // запись элемента с обновленными данными
        {
            if((listData->at(i).id == wid) and (wid != -1)) // поиск нужного элемента по id
            {
                listData->replace(i, *tmp);
                break;
            }
        }
        updateList(wid); // рефреш виджета списка
        delete tmp;
    }

}

void MainWindow::enableui(bool en){
    ui->cGaz->setEnabled(en);
    ui->cJur->setEnabled(en);
    ui->per->setEnabled(en);
    ui->perEday->setEnabled(en);
    ui->perM1->setEnabled(en);
    ui->perM2->setEnabled(en);
    ui->perW1->setEnabled(en);
    ui->perY1->setEnabled(en);
    ui->perY2->setEnabled(en);
    ui->perY4->setEnabled(en);
    ui->perY6->setEnabled(en);
    ui->fHel->setEnabled(en);
    ui->fInfo->setEnabled(en);
    ui->fModa->setEnabled(en);
    ui->fSad->setEnabled(en);
    ui->fSport->setEnabled(en);
    ui->fTV->setEnabled(en);
    ui->fTravel->setEnabled(en);
    ui->nameId->setEnabled(en);
    ui->numberId->setEnabled(en);
    ui->numberSlider->setEnabled(en);
    ui->moneySlider->setEnabled(en);
    ui->moneycount->setEnabled(en);
    ui->checkBox->setEnabled(en);
    ui->dateEdit->setEnabled(en);
}

void MainWindow::updateList(int idRow) // заполнение списка с сортировкой
{

    std::sort(listData->begin(), listData->end());

    ui->saveList->clear(); // чистка интерфейса
    for(int j = 0; j < listData->size(); j++) // заполнение списка
    {
        Contains *tmp = new Contains(listData->at(j));
        QListWidgetItem*it = new QListWidgetItem(); // предварительное создание итема
        it->setData(CONTAINS_ID, tmp->id);
        it->setText(QString(tmp->name + " | " + QString::number(tmp->money, 'f', 2 )));
        ui->saveList->addItem(it);
        delete tmp;
    }

//    ui->saveList->sortItems(); // сортировка итема

    for(int j = 0; j < ui->saveList->count(); j++) // выделение нужного итема после чистки
    {
        if(idRow == ui->saveList->item(j)->data(CONTAINS_ID).toInt()) {
            ui->saveList->setCurrentRow(j);
            break;
        }
    }
}

int MainWindow::getCorrectRandomId() // рандомайзер id
{
    int random = rand();
    Contains correct;
    foreach(correct, *listData){
        if(random == correct.id) getCorrectRandomId();
    }
    return random;
};

void MainWindow::on_saveList_currentRowChanged(int currentRow)  // чтение данных из выделенного элемента списка
{
    accept_read = false;
    if(currentRow != (-1) and(savelist_read))   // если список не будет пустой, тогда и будет чтение
    {
        Contains *tmp;
        for(int i = 0; i < listData->size(); i++) // поиск элемента с нужным id и затем его чтение
        {
            if(listData->at(i).id == ui->saveList->currentItem()->data(CONTAINS_ID)){
                tmp = new Contains(listData->at(i));
                if(tmp->id == -1) enableui(false);
                else enableui(true);
                wid = tmp->id;
                readData(tmp);
                delete tmp;
                break;
            }
        }
    }
    accept_read = true;
}

// Слот:Изменение видимости виджетов QRatioButton при критерии:журнал
void MainWindow::on_cJur_pressed()
{
    uncheckAllPer();// чистим все qRB от прошлых данных
    ui->perEday->setEnabled(true);//делаем нужные qRB видимыми
    ui->perW1->setEnabled(true);
    ui->perM2->setEnabled(true);
    ui->perM1->setEnabled(true);

    ui->fHel->setEnabled(true);
    ui->fInfo->setEnabled(true);
    ui->fModa->setEnabled(true);
    ui->fSad->setEnabled(true);
    ui->fSport->setEnabled(true);
    ui->fTravel->setEnabled(true);

    ui->perEday->setCheckable(true);//даем возможноть выбирать варианты для нужных qRB
    ui->perW1->setCheckable(true);
    ui->perM2->setCheckable(true);
    ui->perM1->setCheckable(true);

    ui->fHel->setCheckable(true);
    ui->fInfo->setCheckable(true);
    ui->fModa->setCheckable(true);
    ui->fSad->setCheckable(true);
    ui->fSport->setCheckable(true);
    ui->fTravel->setCheckable(true);

    ui->moneycount->setMaximum(50000);//изменяем диапазон для выбора стоимости
    ui->moneycount->setMinimum(1500);

    ui->moneySlider->setMaximum(50000);
    ui->moneySlider->setMinimum(1500);

    if (ui->moneycount->text().toInt() <1500 and ui->moneycount->text().toInt()>50000)//проверка соответствия стоимости по критерию
    {   ui->moneycount->setValue(1500);
        ui->moneySlider->setValue(1500);
    }
}

// Слот:Изменение видимости виджетов QRatioButton при критерии:газета
void MainWindow::on_cGaz_pressed()
{
    uncheckAllPer();// чистим все qRB от прошлых данных
    ui->perM2->setEnabled(true);//делаем нужные qRB видимыми
    ui->perM1->setEnabled(true);
    ui->perY6->setEnabled(true);
    ui->perY4->setEnabled(true);
    ui->perY2->setEnabled(true);
    ui->perY1->setEnabled(true);

    ui->fHel->setEnabled(true);
    ui->fInfo->setEnabled(true);
    ui->fModa->setEnabled(true);
    ui->fSad->setEnabled(true);
    ui->fSport->setEnabled(true);
    ui->fTV->setEnabled(true);
    ui->fTravel->setEnabled(true);

    ui->perM2->setCheckable(true);//даем возможноть выбирать варианты для нужных qRB
    ui->perM1->setCheckable(true);
    ui->perY6->setCheckable(true);
    ui->perY4->setCheckable(true);
    ui->perY2->setCheckable(true);
    ui->perY1->setCheckable(true);

    ui->fHel->setCheckable(true);
    ui->fInfo->setCheckable(true);
    ui->fModa->setCheckable(true);
    ui->fSad->setCheckable(true);
    ui->fSport->setCheckable(true);
    ui->fTV->setCheckable(true);
    ui->fTravel->setCheckable(true);

    ui->moneycount->setMaximum(5000);//изменяем диапазон для выбора стоимости
    ui->moneycount->setMinimum(500);
    ui->moneySlider->setMaximum(5000);
    ui->moneySlider->setMinimum(500);

    if (ui->moneycount->text().toInt() <500 and ui->moneycount->text().toInt()>5000)//проверка соответствия стоимости
    {   ui->moneycount->setValue(500);
        ui->moneySlider->setValue(500);
    }
}

// Отключение виджетов RatioButton, зависимые от выбора критерия
void MainWindow::uncheckAllPer() {
    ui->perEday->setEnabled(false);//выключаем qRB
    ui->perW1->setEnabled(false);
    ui->perM2->setEnabled(false);
    ui->perM1->setEnabled(false);
    ui->perY6->setEnabled(false);
    ui->perY4->setEnabled(false);
    ui->perY2->setEnabled(false);
    ui->perY1->setEnabled(false);

    ui->fHel->setEnabled(false);
    ui->fInfo->setEnabled(false);
    ui->fModa->setEnabled(false);
    ui->fSad->setEnabled(false);
    ui->fSport->setEnabled(false);
    ui->fTV->setEnabled(false);
    ui->fTravel->setEnabled(false);

    ui->perEday->setCheckable(false);//убираем возможность выбирать qRB и чистим выборы в них
    ui->perW1->setCheckable(false);
    ui->perM2->setCheckable(false);
    ui->perM1->setCheckable(false);
    ui->perY6->setCheckable(false);
    ui->perY4->setCheckable(false);
    ui->perY2->setCheckable(false);
    ui->perY1->setCheckable(false);

    ui->fHel->setCheckable(false);
    ui->fInfo->setCheckable(false);
    ui->fModa->setCheckable(false);
    ui->fSad->setCheckable(false);
    ui->fSport->setCheckable(false);
    ui->fTV->setCheckable(false);
    ui->fTravel->setCheckable(false);
}

// Запись данных интерфейса в объект класса
void MainWindow::writeData(Contains *tmp)
{
    tmp->cd = ui->checkBox->isChecked();
    tmp->money = ui->moneycount->value();
    tmp->num = ui->numberId->value();
    tmp->date =  ui->dateEdit->date();
    tmp->name = ui->nameId->text();
    if (ui->cJur->isChecked()) (tmp->cat=containsCategory::JURNAL);
    if(ui->cGaz->isChecked())(tmp->cat=containsCategory::GAZETA);

    if (ui->perEday->isChecked()) (tmp->per = containsPeriod::EVERYDAY);
    if (ui->perW1->isChecked()) (tmp->per = containsPeriod::WEAK1);
    if (ui->perM2->isChecked()) (tmp->per = containsPeriod::MONTH2);
    if (ui->perM1->isChecked()) tmp->per = containsPeriod::MONTH1;
    if (ui->perY6->isChecked()) tmp->per = containsPeriod::YEAR6;
    if (ui->perY4->isChecked()) tmp->per = containsPeriod::YEAR4;
    if (ui->perY2->isChecked()) tmp->per = containsPeriod::YEAR2;
    if (ui->perY1->isChecked()) tmp->per = containsPeriod::YEAR1;

    if (ui->fInfo->isChecked()) (tmp->format = containsFormat::INFO);
    if (ui->fModa->isChecked()) (tmp->format = containsFormat::MODA);
    if (ui->fSport->isChecked()) (tmp->format = containsFormat::SPORT);
    if (ui->fHel->isChecked()) (tmp->format = containsFormat::HEALTH);
    if (ui->fSad->isChecked()) (tmp->format = containsFormat::GARDEN);
    if (ui->fTravel->isChecked()) (tmp->format = containsFormat::TRAVEL);
    if (ui->fTV->isChecked()) (tmp->format = containsFormat::TV);
}

//Заполнение интерфейса данными из объекта класса
void MainWindow::readData(Contains *tmp)
{
    ui->checkBox->setChecked(tmp->cd);
    ui->moneycount->setValue(tmp->money);
    ui->numberId->setValue(tmp->num);
    ui->dateEdit->setDate(tmp->date);
    ui->nameId->setText(tmp->name);

    bool accept = true;
    uncheckAllPer();
    if (tmp->cat==containsCategory::JURNAL) {
        ui->cJur->setChecked(true);
        on_cJur_pressed();
    }
    if(tmp->cat==containsCategory::GAZETA){
        ui->cGaz->setChecked(true);
        on_cGaz_pressed();
    }
    if(tmp->cat==containsCategory::CNULL){
        ui->cGaz->setAutoExclusive(false);
        ui->cJur->setAutoExclusive(false);
        ui->cGaz->setChecked(false);
        ui->cJur->setChecked(false);
        ui->cGaz->setAutoExclusive(true);
        ui->cJur->setAutoExclusive(true);
        accept = false;
    }

    if(accept){
        if (tmp->per == containsPeriod::EVERYDAY) ui->perEday->setChecked(true);
        if (tmp->per == containsPeriod::WEAK1) ui->perW1->setChecked(true);
        if (tmp->per  == containsPeriod::MONTH2) ui->perM2->setChecked(true);
        if (tmp->per == containsPeriod::MONTH1) ui->perM1->setChecked(true);
        if (tmp->per == containsPeriod::YEAR6) ui->perY6->setChecked(true);
        if (tmp->per == containsPeriod::YEAR4) ui->perY4->setChecked(true);
        if (tmp->per == containsPeriod::YEAR2) ui->perY2->setChecked(true);
        if (tmp->per == containsPeriod::YEAR1) ui->perY1->setChecked(true);

        if (tmp->format == containsFormat::INFO) ui->fInfo->setChecked(true);
        if (tmp->format == containsFormat::MODA) ui->fModa->setChecked(true);
        if (tmp->format == containsFormat::SPORT) ui->fSport->setChecked(true);
        if (tmp->format == containsFormat::HEALTH) ui->fHel->setChecked(true);
        if (tmp->format == containsFormat::GARDEN) ui->fSad->setChecked(true);
        if (tmp->format == containsFormat::TRAVEL) ui->fTravel->setChecked(true);
        if (tmp->format == containsFormat::TV) ui->fTV->setChecked(true);
    }
}

//4 слота для взаимной работы слайдера+поля ввода денег и номера индекса
void MainWindow::on_numberSlider_sliderMoved(int position)
{
    ui->numberId->setValue(position);
}
void MainWindow::on_moneySlider_sliderMoved(int position)
{
    ui->moneycount->setValue(position);
}
void MainWindow::on_numberId_valueChanged(int arg1)
{
    ui->numberSlider->setValue(arg1);
}
void MainWindow::on_moneycount_valueChanged(double arg1)
{
    ui->moneySlider->setValue(int(arg1));
}

// Подтверждение выхода
void MainWindow::on_clCancel_clicked()
{
    QMessageBox::StandardButton exit = QMessageBox::question(this, "Внимание", "Вы действительно хотите выйти?");
    if (exit == QMessageBox::Yes) QApplication::exit();
}
void MainWindow::closeEvent(QCloseEvent *cEvent){
    QMessageBox::StandardButton wquit = QMessageBox::question(this,"Внимание", "Вы действительно хотите выйти?");
    if (wquit == QMessageBox::Yes){
        cEvent->accept();
    }
    else cEvent->ignore();
}


void MainWindow::on_nameId_textChanged(const QString &arg1)
{    if(accept_read)savechanges();
}

void MainWindow::on_cJur_clicked()
{    if(accept_read)savechanges();
}

void MainWindow::on_cGaz_clicked()
{    if(accept_read)savechanges();
}

void MainWindow::on_numberId_valueChanged(const QString &arg1)
{    if(accept_read)savechanges();
}

void MainWindow::on_numberSlider_actionTriggered(int action)
{    if(accept_read)savechanges();
}

void MainWindow::on_perEday_clicked()
{    if(accept_read)savechanges();
}

void MainWindow::on_perW1_clicked()
{   if(accept_read)savechanges();
}

void MainWindow::on_perM2_clicked()
{    if(accept_read)savechanges();
}

void MainWindow::on_perM1_clicked()
{    if(accept_read)savechanges();
}

void MainWindow::on_perY6_clicked()
{    if(accept_read)savechanges();
}

void MainWindow::on_perY2_clicked()
{    if(accept_read)savechanges();
}

void MainWindow::on_perY4_clicked()
{   if(accept_read)savechanges();
}

void MainWindow::on_perY1_clicked()
{    if(accept_read)savechanges();
}

void MainWindow::on_fInfo_clicked()
{    if(accept_read)savechanges();
}

void MainWindow::on_fModa_clicked()
{    if(accept_read)savechanges();
}

void MainWindow::on_fSport_clicked()
{    if(accept_read)savechanges();
}

void MainWindow::on_fHel_clicked()
{    if(accept_read)savechanges();
}

void MainWindow::on_fSad_clicked()
{    if(accept_read)savechanges();
}

void MainWindow::on_fTravel_clicked()
{    if(accept_read)savechanges();
}

void MainWindow::on_fTV_clicked()
{    if(accept_read)savechanges();
}

void MainWindow::on_moneycount_valueChanged(const QString &arg1)
{    if(accept_read)savechanges();
}

void MainWindow::on_moneySlider_actionTriggered(int action)
{   if(accept_read)savechanges();
}

void MainWindow::on_checkBox_stateChanged(int arg1)
{    if(accept_read)savechanges();
}

void MainWindow::on_dateEdit_userDateChanged(const QDate &date)
{    if(accept_read)savechanges();
}

