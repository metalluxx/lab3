#pragma once
#ifndef CONTAINS_H
#define CONTAINS_H

#include <QString>
#include <QDate>
#include <QVariant>

enum containsCategory {CNULL, JURNAL, GAZETA, };
enum containsPeriod {PNULL, EVERYDAY, WEAK1, MONTH2, MONTH1, YEAR6, YEAR4, YEAR2, YEAR1 };
enum containsFormat {FNULL, INFO, MODA, SPORT, HEALTH, GARDEN, TRAVEL, TV};
Q_DECLARE_METATYPE(containsCategory); // добавление enum's в QVariant
Q_DECLARE_METATYPE(containsPeriod);
Q_DECLARE_METATYPE(containsFormat);

class Contains
{
public:
    explicit Contains();
    ~Contains();
    int id = -1;
    QString name = "";
    int num = 10000;
    double money = 0.0;
    bool cd = false;
    QDate date = QDate::currentDate();
    containsCategory cat = CNULL;
    containsPeriod per = PNULL;
    containsFormat format = FNULL;
    // далее перегрузка операторов сравнения
    friend bool operator < (const Contains &t1, const Contains &t2){
        if(t1.cat == t2.cat){
            if(t1.per == t2.per){
                if(t1.num == t2.num){
                    return false;
                }else return t1.num < t2.num;
            }else return  t1.per < t2.per;
        }else return t1.cat < t2.cat;
}
    friend bool operator <= (const Contains &t1, const Contains &t2){
        if(t1.cat == t2.cat){
            if(t1.per == t2.per){
                if(t1.num == t2.num){
                    return true;
                }else return t1.num <= t2.num;
            }else return  t1.per <= t2.per;
        }else return t1.cat <= t2.cat;
}
    friend bool operator >= (const Contains &t1, const Contains &t2){
        if(t1.cat == t2.cat){
            if(t1.per == t2.per){
                if(t1.num == t2.num){
                    return true;
                }else return t1.num >= t2.num;
            }else return  t1.per >= t2.per;
        }else return t1.cat >= t2.cat;
}
    friend bool operator > (const Contains &t1, const Contains &t2) {
        if(t1.cat == t2.cat){
            if(t1.per == t2.per){
                if(t1.num == t2.num){
                    return false;
                }else return t1.num < t2.num;
            }else return  t1.per > t2.per;
        }else return t1.cat > t2.cat;
    }
    friend bool operator == (const Contains &t1, const Contains &t2) {
        if(t1.cat == t2.cat){
            if(t1.per == t2.per){
                if(t1.num == t2.num){
                    return true;
                }else return false;
            }else return  false;
        }else return false;
    }
    friend bool operator != (const Contains &t1, const Contains &t2) {
        return !operator==(t1,t2);
    }
};

#endif // CONTAINS_H
